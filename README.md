# cat-cafe-connecting-mysql

In this lab, we will learn to pull information from a MySQL database to our front-end React app

# Scenario

Jen has hired you to create a website for her cat cafe! It will be a landing page that shows all the cats available for adoption at her cafe.

You have been given the following materials in the Google Drive link here:
https://docs.google.com/spreadsheets/d/1JR0eDHVfm4Z50dxGVJsHPFzoECaOKbHK2BfExPWC35A/edit?usp=sharing

1. Cat Database
2. Images of the cats to be adopted

# Instructions

Build a landing page using create-react-app and connect a MySQL database. This lab will be divided into 4 parts.

## Part 1 - Creating a Blank Template

We will be building our database, our backend api and our front-end blank tamplate before we connect it all together.

1. Create a new React app 

2. Create a new Gitlab project

3. Git push your React app into your Gitlab repo

4. Make a new branch called `blank-template`

3. Create the cat database based on Jen's Excel Sheet in MySQL

4. npm install `express esm mysql` and `nodemon`

5. Delete everything in the React Template so that way it's a blank page not that generic React landing page design (App.js).

6. `git push` but save the branch incase you need it

## Part 2 - Connecting MySQL

1. Checkout a new branch called `db-connection`

2. (This is an FS1020 review) Create a `server.js` file and have it listen to `port 3001`- React App is listening to port 3000 so use a different port. Your React front end will be taking information from :3001 (see diagram): https://miro.com/app/board/o9J_lZizQCU=/

- Create a `GET("/api")` with a `res.send('hello world')` route to make sure it works!
- add a dev script `"dev": "nodemon -r esm server.js"`
- `npm run dev` to check if it works

3. Connect the database

- create a file `connection.js` that has to the code to connect the db, make sure to export the module
- install and import MySQL: https://www.npmjs.com/package/mysql
- https://www.w3schools.com/nodejs/nodejs_mysql.asp
- to check if connected - run `npm run dev`

4. Create a `GET("/api/cats")` route and pull all cats from the database

- be sure to import the database so you run queries
- how to query your db: https://www.npmjs.com/package/mysql

5. You're now pulling data from MySQL! `Git push` your branch

## Part 3 - Connecting React

1. Checkout a new branch called `connecting-react`

2. In React, create a `CatProfile component` that will be used/imported in `App.js`

- Cat Profile will have the following:

```
  <div>
      <p>Name: Tuna</p>
      <img src="" alt="alt" width="300" height="300/>
    </div>
```

3. `npm install cors` and add a `proxy` to the end of `package.json` so you can fetch api calls from the :3001 api routes we made in part 2
- adding cors: https://www.npmjs.com/package/cors
- adding proxy: https://dev.to/loujaybee/using-create-react-app-with-express

4. Use React Hooks (`useState and useEffect`) to set the initial state, fetch api calls and then set the new state

- Hint: Check out UseEffect at ComponentDidMount section at the very bottom of this article: https://medium.com/better-programming/how-to-fetch-data-from-an-api-with-react-hooks-9e7202b8afcd

5.  `Map` the array from localhost:3001 to your CatProfile component so all cats appear on the page

6.  `git push`

## Part 4 - Add Concurrently

Concurrently allows you to run multiple commands in the terminal when you start your app

1. Pull a new branch and name it `concurrently`

2. Install and edit your `start` script

- https://www.newline.co/fullstack-react/articles/using-create-react-app-with-a-server/
- https://www.npmjs.com/package/concurrently

3. `git push`

## "The client calls you for a meeting"

Jen forgot to tell you that her cat cafe is now accepting new cats to house. She has decided that she also wants to have a page where anyone can register a adoptable cat to her website and drop it off at the shop later.

## Part 5 - React Routes

In order to add more pages to our front-end, we'll need to install React router.

You can learn more about it here:
https://reactrouter.com/web/api/BrowserRouter

1. Pull a new branch and name it `react-router`

2. `npm install --save react-router-dom`

3. `import { BrowserRouter } from "react-router-dom"` to `index.js` and add `<BrowserRouter>` between your `<App>` tags

4. `import { Route } from "react-router-dom"` in `App.js` and direct your CatProfile component to '/cats' route `<Route exact path="/cats" component={CatProfile} />`

5. Create another component `AddCatForm` and route it `<Route exact path="/add-cat" component={AddCatForm} />`

Component info below:

```<div>
      <h1>This is the add cat form</h1>
      <form>
        <div>
          <label>
            Name:
            <input
              type="text"
              name="name"
            />
          </label>
        </div>
        <div>
          <label>
            Photo:
            <input
              type="text"
              name="image"
            />
          </label>
        </div>
        <input type="submit" value="Submit" />
      </form>
    </div>
```

6. `git push`

## Part 6 - API POST route to MySQL

Let's create a route that will post to our MySQL database

1. Pull a new branch and call it `posting-db`

2. Create a `POST /api/cats` request to add a new cat to the database in `server.js`

- https://www.w3schools.com/nodejs/nodejs_mysql_insert.asp

3. Check on Postman if it works

4. Use React Hooks (`useState`) to set the initial state, fetch post api call and then set the new state.

- set the initial state with `useState`

`const [newCat, setNewCat] = useState({ name: "", image: "", });`

- create a `handleChange` function to change the state

` const handleChange = (event) => { setNewCat((prevState) => ({ ...prevState, [event.target.name]: event.target.value, })); };`

- connect the form values and check dev tools to see if state is changing (hint: `onChange`)
- https://reactjs.org/docs/forms.html

- create a `handleSubmit` function to `fetch` post and connect to form (hint: `onSubmit`)
- https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

```const handleSubmit = () => {
    fetch("/api/cats", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(newCat),
    }).then((response) => response.json());
  };
```

5. `git push`
